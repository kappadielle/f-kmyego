import { Injectable } from '@angular/core';
import { auth } from 'firebase/app';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs';
import { DatabaseService } from './database.service';
import { filter } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  currentUser = null;
  constructor(private afAuth: AngularFireAuth, private DBSer: DatabaseService, private router: Router) {
    afAuth.authState.pipe(
      filter(val => val != null)
    ).subscribe(userData => {
      this.currentUser = userData;
      this.DBSer.getUser(userData.uid).subscribe(_ => {
        console.log(this.DBSer.week)
        this.router.navigate(['/table']);
      });
    });
  }
  googleLogin() {
    const provider = new auth.GoogleAuthProvider();
    return this.socialSignIn(provider);
  }
  socialSignIn(provider: auth.GoogleAuthProvider) {
    return this.afAuth.auth.signInWithPopup(provider);
  }
}
