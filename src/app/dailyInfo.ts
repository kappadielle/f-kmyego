export interface DailyInfo {
    data: any;
    EN: number;
    EI: number;
    ED: number;
    EP: number;
    EN1?: number;
    EI1?: number;
    ED1?: number;
    EP1?: number;
}
