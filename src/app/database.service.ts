import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { DailyInfo } from './dailyInfo';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {
  currentUser: User;
  weekView = 0;
  week = [];
  numEgo = 1;
  mod = 0;
  init = {
    dati: [
      {
        data : new Date(new Date().setHours(0, 0, 0, 0)),
        EN : 0,
        EI : 0,
        ED : 0,
        EP : 0,
        EN1 : 0,
        EI1 : 0,
        ED1 : 0,
        EP1 : 0
      }
    ]
  };
  lastDay: Date;
  constructor(private DB: AngularFirestore) { }

  getUser(val: string): Observable<User> {
    return this.DB.doc('user/' + val).snapshotChanges().pipe(
      take(1),
      map(snap => {
        let id = null;
        let data = null;
        if (snap.payload.exists) {
          data = snap.payload.data();
          id   = snap.payload.id;
          this.fillDataDay({ id, ...data });
          this.lastDay = new Date(data.dati[data.dati.length - 1].data.seconds * 1000);
        } else {
          this.DB.doc('user/' + val).set(this.init);
          id = val;
          data = this.init;
          this.lastDay = data.dati[data.dati.length - 1].data;
        }
        this.currentUser = { id, ...data };
        this.fillObjWeek(this.weekView);
        return { id, ...data };
      })
    );
  }
  fillDataDay(obj): any {
    // datiGiorno = obj.data.dati[0].data;
    const dataReset = new Date().setHours(0, 0, 0, 0);
    if (obj.dati[obj.dati.length - 1].data.seconds * 1000 !== dataReset ) {
      obj.dati.push(this.init.dati[0]);
      this.updateDocument(`user/${obj.id}`, obj);
      console.log('ci sono');
    }
  }
  cambiaOperazione(op: string) {
    if (op === '+') {
      this.numEgo = 1;
    } else if (op === '-') {
      this.numEgo = -1;
    }
  }
  aggiungiEgo(item: DailyInfo, ego: string): any {
    if (this.mod === 0) {
      switch (ego) {
        case 'EI' : {
          item.EI += this.numEgo;
        } break;
        case 'EN' : {
          item.EN += this.numEgo;
        } break;
        case 'EP' : {
          item.EP += this.numEgo;
        } break;
        case 'ED' : {
          item.ED += this.numEgo;
        }
      }
    } else if (this.mod === 1) {
      switch (ego) {
        case 'EI' : {
          item.EI1 += this.numEgo;
        } break;
        case 'EN' : {
          item.EN1 += this.numEgo;
        } break;
        case 'EP' : {
          item.EP1 += this.numEgo;
        } break;
        case 'ED' : {
          item.ED1 += this.numEgo;
        }
      }
    }
    for (let j of this.currentUser.dati) {
      if (j.data.seconds * 1000 == item.data.getTime()) {
        j.ED = item.ED;
        j.EI = item.EI;
        j.EN = item.EN;
        j.EP = item.EP;
        j.ED1 = item.ED1;
        j.EI1 = item.EI1;
        j.EN1 = item.EN1;
        j.EP1 = item.EP1;
        // aggiorna currentUser
      }
    }
    console.log(this.currentUser)
    this.updateDocument('user/' + this.currentUser.id, this.currentUser);
  }
  cambiaMod(op: number) {
    if (op === 0) {
      this.mod = 0;
    } else if (op === 1) {
      this.mod = 1;
    }
  }
  fillObjWeek(showWeek) {
    this.week = [];
    // tslint:disable-next-line:max-line-length
    const lastDay = new Date(this.lastDay.getTime() + (showWeek * (86400000 * 7)));
    const lastDayWeekNum = lastDay.getDay();
    if (lastDayWeekNum !== 1 && lastDayWeekNum !== 0) {
      let num = 1;
      const lunedi = this.sottraiNGiorni(lastDayWeekNum - num, lastDay);
      this.week.push({data: lunedi});
      do {
        this.week.push({data: this.addNGiorni(num, lunedi)});
        num ++;
      } while (num <= 6);
    } else if (lastDayWeekNum === 0) {
      const dom = lastDay;
      let num = 1;
      do {
        this.week.push({data: this.sottraiNGiorni(7 - num, dom)});
        num ++;
      } while (num <= 6);
      this.week.push({data: dom});
    } else if (lastDayWeekNum === 1) {
      const dom = this.sottraiNGiorni(1, lastDay);
      let num = 1;
      do {
        this.week.push({data: this.addNGiorni(num, dom)});
        num ++;
      } while (num <= 6);
      this.week.push({data: this.addNGiorni(7, dom)});
    }
    let i = 0;
    for (const item of this.week) {
      for (const j of this.currentUser.dati) {
        if (j.data.seconds * 1000 === new Date(item.data).getTime()) {
          // j.data = new Date(j.data.seconds * 1000);
          this.week[i].data = new Date(j.data.seconds * 1000);
          this.week[i].EI = j.EI;
          this.week[i].EN = j.EN;
          this.week[i].EP = j.EP;
          this.week[i].ED = j.ED;
          this.week[i].EI1 = j.EI1;
          this.week[i].EN1 = j.EN1;
          this.week[i].EP1 = j.EP1;
          this.week[i].ED1 = j.ED1;
        }
      }
      i ++;
    }
  }
  cambiaWeekView(op: string) {
    if (op === '+') {
      this.weekView ++;
    } else if (op === '-') {
      this.weekView --;
    } else if (op === '0') {
      this.weekView = 0;
    }
    this.fillObjWeek(this.weekView);
  }
  sottraiNGiorni(numGiorni: number, date: Date): Date {
    return new Date(date.getTime() - (86400000 * numGiorni));
  }
  addNGiorni(numGiorni: number, date: Date): Date {
    return new Date(date.getTime() + (86400000 * numGiorni));
  }
  updateDocument(val: string, data: User) {
    this.DB.doc(val).update(data);
  }
}
