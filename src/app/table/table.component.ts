import { Component, OnInit } from '@angular/core';
import { DatabaseService } from '../database.service';
import { User } from '../user';
import { DailyInfo } from '../dailyInfo';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  currentUser: User;
  constructor(public DBSer: DatabaseService) { }

  ngOnInit() {
  }
  aggiungiEgo(item: DailyInfo, ego: string) {
    this.DBSer.aggiungiEgo(item, ego);
  }
  cambiaWeekView(op: string) {
    this.DBSer.cambiaWeekView(op);
  }
  addTodayClass(data): string {
    const dataReset = new Date().setHours(0, 0, 0, 0);
    let giorno = '';
    if (data.getTime() == dataReset) {
      giorno = 'today';
    }
    return giorno;
  }
  cambiaOperazione(op: string) {
    this.DBSer.cambiaOperazione(op);
  }
  cambiaMod(op: number) {
    this.DBSer.cambiaMod(op);
  }
}
