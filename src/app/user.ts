import { DailyInfo } from './dailyInfo';

export interface User {
    id?: string;
    dati: DailyInfo[];
}
